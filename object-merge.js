/////////////////////////////////////////////////////////////////////////////////////////////
//
// object-merge
//
//    Library for merging javascript objects.
//
// License
//    Apache License Version 2.0
//
// Copyright Nick Verlinden (info@createconform.com)
//
///////////////////////////////////////////////////////////////////////////////////////////// 
/////////////////////////////////////////////////////////////////////////////////////////////
//
// Privates
//
/////////////////////////////////////////////////////////////////////////////////////////////
var Error = require("error");
var type =  require("type");
var clone = require("object-clone");

var ERROR_INVALID_OBJECT = "Invalid Object";

/////////////////////////////////////////////////////////////////////////////////////////////
//
// Functions
//
/////////////////////////////////////////////////////////////////////////////////////////////
function merge(source1, source2, overwriteArray) {
    /*
    * Properties from the Source1 object will be copied to Source2 Object. Source1 will overwrite any existing attributes
    * Note: This method will return a new merged object, Source1 and Source2 original values will not be replaced.
    * */
    if(source1 == null || !type.isObject(source1)){
        throw new Error(ERROR_INVALID_OBJECT, "Parameter 'source1' is of type '" + (typeof source1) + "', while null or an object is expected.");
    }
    if(source2 == null || !type.isObject(source2)){
        throw new Error(ERROR_INVALID_OBJECT, "Parameter 'source2' is of type '" + (typeof source2) + "', while null or an object is expected.");
    }

    var mergedJSON = clone(source2);

    for (var attrname in source1) {
        if(mergedJSON.hasOwnProperty(attrname)) {
            if ( source1[attrname]!=null) {
                if (source1[attrname].constructor==Object) {
                    /*
                    * Recursive call if the property is an object,
                    * Iterate the object and set all properties of the inner object.
                    */
                    mergedJSON[attrname] = merge(source1[attrname], mergedJSON[attrname], overwriteArray);
                }
                else if (type.isArray(source1[attrname]) && !overwriteArray) {
                    mergedJSON[attrname] = mergedJSON[attrname].concat(source1[attrname]);
                }
                else {
                    mergedJSON[attrname] = source1[attrname];
                }
            }
        } else {
            //else copy the property from source1
            mergedJSON[attrname] = source1[attrname];
        }
    }

    return mergedJSON;
}

/////////////////////////////////////////////////////////////////////////////////////////////
module.exports = merge;
module.exports.ERROR_INVALID_OBJECT = ERROR_INVALID_OBJECT;